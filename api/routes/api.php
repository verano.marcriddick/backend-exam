<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::namespace('Auth')->group(function () {
        Route::post('logout', 'LoginController@logout');
    });
});

Route::apiResource('posts', 'PostController');
Route::apiResource('posts.comments', 'CommentController');

Route::namespace('Auth')->group(function () {
	Route::post('login', 'LoginController@authenticate');
	Route::post('register', 'RegisterController@register');
});