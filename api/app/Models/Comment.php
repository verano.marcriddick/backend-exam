<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory, SoftDeletes;

   	/**
     * Get the parent post of comment.
     */
    public function user()
    {
        return $this->belongsTo(Post::class);
    }
}
