<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
        'title',
        'slug',
        'content',
        'image'
    ];

    /**
     * Get the comments of the post.
     */
    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get the owner of the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
