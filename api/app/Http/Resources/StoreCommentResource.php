<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'body' => $this->body,
            'commentable_type' => $this->commentable_type,
            'commentable_id' => $this->commentable_id,
            'creator_id' => $this->creator_id,
            'parent_id' => $this->parent_id,
            'updated_at' => Carbon::parse($this->updated_at)->toDateTimeString(),
            'created_at' => Carbon::parse($this->created_at)->toDateTimeString(),
            'id' => $this->id
        ];
    }
}
