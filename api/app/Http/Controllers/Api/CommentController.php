<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Resources\ShowCommentResource;
use App\Http\Resources\StoreCommentResource;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * CommentController Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($title)
    {
        $code = 200;
        $post = Post::where('title', $title)->first();
        $result['data'] = Comment::where('parent_id', $post->id)->get();

        return response()->json($result, $code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request, $title)
    {
        $data = $request->only('body');
        $user = Auth()->user();
        $post = Post::where('title', $title)->first();

        if (!($post instanceof Post)) {
            $code = 404;
            $result['message'] = 'Unable to perform action. Post doesn\'t exist.';
        } else {
            $comment = new Comment;
            $comment->body = $data['body'];
            $comment->commentable_type = 'App\\Models\\Post';
            $comment->commentable_id = 1;
            $comment->parent_id = $post->id;
            $comment->creator_id = $user->id;
            $comment->save();

            $code = 201;
            $result['data'] = new StoreCommentResource($comment);
        }

        return response()->json($result, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $title, $comment_id)
    {
        $data = $request->only('body');
        $user = Auth::user();
        $comment = Comment::where('id', $comment_id)
            ->where('creator_id', $user->id)
            ->first();

        if (!($comment instanceof Comment)) {
            $code = 404;
            $result['message'] = 'Unable perform action. Comment doesn\'t exist.';
        } else {
            $code = 200;
            $comment->body = $data['body'];
            $comment->save();

            $result['data'] = new ShowCommentResource($comment);
        }

        return response()->json($result, $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($title, $comment_id)
    {
        $user = Auth::user();
        $comment = Comment::where('id', $comment_id)
            ->where('creator_id', $user->id)
            ->first();

        if (!($comment instanceof Comment)) {
            $code = 404;
            $result['message'] = 'Unable perform action. Comment doesn\'t exist.';
        } else {
            $code = 200;
            $comment->delete();
            $result['status'] = 'Comment deleted successfully.';
        }

        return response()->json($result, $code);
    }
}
