<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Create a new user.
     * 
     * @param Request $request
     * @return void
     */
    public function authenticate(LoginRequest $request)
    {
        $data = $request->only(
            'email',
            'password'
        );

        if (!Auth::attempt($data)) {
            return $this->sendFailedLoginResponse($request);
        } else {
            $token_result = Auth::user()->createToken('authToken');
            $token = $token_result->token;
            $token->expires_at = Carbon::now()->addDay();
            $token->save();

            $result = [
                'token' => $token_result->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $token_result->token->expires_at
                )->toDateTimeString()
            ];
        }

        return response()->json($result, 200);
    }

    /**
     * Logout the authenticated user.
     * 
     * @param Request $request
     * @return json
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $code = 200;
        $result['message'] = 'You have logged out successfully.';

        return response()->json($result, $code);
    }
}
