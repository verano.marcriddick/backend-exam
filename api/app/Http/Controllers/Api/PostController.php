<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Resources\ShowPostResource;
use App\Http\Resources\StorePostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * PostController Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->only('limit');
        $limit = isset($data['limit']) ? isset($data['limit']) : 0;
        $posts = Post::orderBy('created_at', 'desc')->paginate($limit);

        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $data = $request->only(
            'title',
            'content',
            'image'
        );
        $user = Auth()->user();
        $post = Post::where('user_id', $user->id)
            ->where('title', $data['title'])
            ->first();

        if (!($post instanceof Post)) {
            $post = new Post;
            $post->user_id = $user->id;
        }

        $post->title = $data['title'];
        $post->slug = $data['title'];
        $post->content = $data['content'];
        $post->image = isset($data['image']) ? $data['image'] : null;
        $post->save();

        $result['data'] = new StorePostResource($post);

        return response()->json($result, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        $post = Post::where('title', $title)->first();

        if (!($post instanceof Post)) {
            $code = 404;
            $result['message'] = 'No query results for model [App\Models\Post].';
        } else {
            $code = 200;
            $result['data'] = new ShowPostResource($post);
        }

        return response()->json($result, $code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $title)
    {
        $data = $request->only(
            'title',
            'content'
        );
        $user = Auth::user();
        $post = Post::where('title', $title)
            ->where('user_id', $user->id)
            ->first();

        if (!($post instanceof Post)) {
            $code = 404;
            $result['message'] = 'Unable perform action. Post doesn\'t exist.';
        } else {
            $code = 200;
            $post->title = $data['title'];
            $post->slug = $data['title'];
            $post->content = isset($data['content']) ? $data['content'] : '';
            $post->save();

            $result['data'] = new ShowPostResource($post);
        }

        return response()->json($result, $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($title)
    {
        $user = Auth::user();
        $post = Post::where('title', $title)
            ->where('user_id', $user->id)
            ->first();

        if (!($post instanceof Post)) {
            $code = 404;
            $result['message'] = 'Unable perform action. Post doesn\'t exist.';
        } else {
            $code = 200;
            $post->delete();
            $result['status'] = 'Post deleted successfully.';
        }

        return response()->json($result, $code);
    }
}
