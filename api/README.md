
# LIG Blog API

## Features:
- Register
- Login/logout
- Insert, update, delete posts
- Insert, update, delete comments
https://documenter.getpostman.com/view/78990/RznLHGgv

## Installation

 1. Clone the repository
 2. Go to api/ folder
 3. Install dependencies

	    composer install

 4. Run the database migrations

	    php artisan migrate
> **Note:** Make your .env file has the correct configurations for your database connection.

 5. I'm using Laravel Passport for authentication. So please run this command:

	    php artisan passport:install
    
 6. Before running the backend server, run this command:
 
		 php artisan config:clear
    
 7. Once all steps are done above, you're ready to run your local environment for PHP.

	    php artisan serve

 8. Go back to the base folder and run.

	    npm i
9. Then lastly, run the following command:
		
		npm start
 
You may now access the Blog in http://localhost:3000/.


Laravel version used: 8.22.1
PHP version used: 7.4.11
Database: XAMPP
> **Note:** You're free to choose the technology you're comfortable with.